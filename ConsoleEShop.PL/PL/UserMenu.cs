﻿using ConsoleEShop.BLL.DTOs;
using ConsoleEShopMultilayered.DAL.Entities;
using ConsoleEShopMultilayered.DAL.UoW;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;


namespace ConsoleEShopMultilayered.PL
{
    public class UserMenu : BaseMenu
    {

        protected UserDto user;
        public UserMenu(UnitOfWork uow, UserDto user) : base(uow)
        {
            this.user = user;
            Commands = new Dictionary<int, Action>()
            {
                { 1, PrintPoductList },
                { 2, SearchProduct },
                { 3, ShowAndCreate },
                { 4, ShowAllOrders},
                { 5, ChangePersonalInfo},
                { 6, ChangeOrderStatus},
                { 7, LogOut}
            };
        }

        public override void PrintMenu()
        {
            Console.WriteLine($"You loggined as {user.Login}");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("-- Menu");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("1. Show all products");
            Console.WriteLine("2. Search product by name");
            Console.WriteLine("3. Create new order");
            Console.WriteLine("4. Order history and delivery status");
            Console.WriteLine("5. Change personal info");
            Console.WriteLine("6. Change order status");
            Console.WriteLine("7. Log out");
        }

        public override void GetUserInput(string data)
        {
            int parsed = 0;
            int.TryParse(data, out parsed);

            if (Commands.ContainsKey(parsed))
            {
                Commands[parsed].Invoke();
            }
        }

        public void ShowAllOrders()
        {
            Console.Clear();
            var orders = OrderService.GetUserOrders(user.Id).ToList();
            if (orders == null || orders.Count == 0)
            {
                Console.WriteLine("You have no orders!");
                Console.ReadKey();
                return;
            }
            Console.WriteLine("\t\t\t\t\t-- Your orders --");
            foreach (var i in orders)
            {
                Console.WriteLine(i + "\n");
            }
            Console.ReadKey();
        }

        public override void Execute()
        {
            PrintMenu();
            Console.Write("Enter: ");
            GetUserInput(Console.ReadLine());
        }

        public void ChangeOrderStatus()
        {
            //do
            //{
            //    var hasOrders = PrintOrders();
            //    if (!hasOrders)
            //        return;
            //    Console.WriteLine("\n1 - Set status \"Received\"");
            //    Console.WriteLine("2 - Set status \"Canceled\"");
            //    Console.WriteLine("3 - Back to menu");
            //    int choice; Order order;
            //    Console.Write("Enter: ");
            //    choice = int.Parse(Console.ReadLine());
            //    var success = false;
            //    switch (choice)
            //    {
            //        case 1:
            //            Console.Write("-- Enter order id to set status: ");
            //            choice = int.Parse(Console.ReadLine());
            //            order = OrderService.Find(i=> i.Id == choice);
            //            success = user.SetStatusReceived(order); break;
            //        case 2:
            //            Console.Write("-- Enter order number to set status: ");
            //            choice = int.Parse(Console.ReadLine());
            //            order = OrderService.Find(i => i.Id == choice); 
            //            success = user.SetStatusCanceled(order); break;
            //        case 3: return;
            //        default: break;
            //    }
            //    if (!success)
            //    {
            //        Console.WriteLine("You can't set this status to order!");
            //    }
            //    else
            //    {
            //        Console.WriteLine("Status changed successfully!");
            //    }
            //    Console.ReadKey();
            //} while (true);
        }

        public void ChangePersonalInfo()
        {
            EditUserInfo(user);
            UserService.Update(user);
        }

        public void EditUserInfo(UserDto account)
        {
            do
            {
                Console.Clear();
                Console.WriteLine("\t\t\t\t-- Personal information --");
                Console.WriteLine(account);

                Console.WriteLine("\n--What to change");
                Console.WriteLine("1 - Name ");
                Console.WriteLine("2 - Email ");
                Console.WriteLine("3 - Phone number ");
                Console.WriteLine("4 - Back");
                Console.Write("Enter: ");
                int choice;
                int.TryParse(Console.ReadLine(), out choice);
                switch (choice)
                {
                    case 1:
                        Console.Write("Enter new Name: ");
                        account.Name = Console.ReadLine();
                        break;
                    case 2:
                        Console.Write("Enter new Email: ");
                        account.contact.Email = Console.ReadLine();
                        break;
                    case 3:
                        Console.Write("Enter new Phone number: ");
                        account.contact.PhoneNumber = Console.ReadLine();
                        break;
                    case 4: return;
                    default: break;
                }
            } while (true);
        }

        public bool PrintOrders()
        {
            var orders = GetUserOrders();
            Console.Clear();
            if (orders == null || orders.Count == 0)
            {
                Console.WriteLine("You have no orders!");
                Console.ReadKey();
                return false;
            }
            Console.WriteLine("\t\t\t\t\t-- Your orders --");
            foreach (var i in orders)
            {
                Console.WriteLine(i);
                Console.WriteLine();
            }
            return true;
        }

        private List<OrderDto> GetUserOrders()
        {
            var ordersId = user.OrdersId;
            List<OrderDto> orders = new List<OrderDto>();
            foreach (var id in ordersId)
            {
                orders.Add(OrderService.Find(i => i.Id == id));
            }
            return orders;
        }

        public void LogOut()
        {
            LogOutNotify?.Invoke(user, new EventArgs());
        }

        public void ShowAndCreate()
        {
            base.PrintPoductList();
            Console.Write("Do you want to make an order?(y/n- go back to menu): ");
            string input = Console.ReadLine();
            if (input == "y")
            {
                CreateNewOrder();
            }
        }



        public void CreateNewOrder()
        {
            List<OrderItemDto> orderItems = new List<OrderItemDto>();
            int productNum;
            do
            {
                Console.Clear();
                base.PrintPoductList();
                Console.Write("-- Enter product id to create order: ");
                int.TryParse(Console.ReadLine(), out productNum);

                var foundProduct = ProductService.Find(i => i.Id == productNum);

                if (foundProduct == null)
                {
                    Console.WriteLine($"Product with id:{productNum} is not exist");
                    Console.ReadKey();
                    continue;
                }

                Console.Write("-- Enter count of products: ");
                int count;
                int.TryParse(Console.ReadLine(), out count);

                orderItems.Add(new OrderItemDto() { Product = foundProduct, Quantity = count });
                Console.WriteLine("\n1 - Proceed to oder checkout");
                Console.WriteLine("2 - Add more items to card");
                Console.Write("Enter: ");
                int choice; int.TryParse(Console.ReadLine(), out choice);
                if (choice == 1) break;

            } while (true);
            Console.Clear();
            Console.WriteLine("-- Your card");
            Console.ForegroundColor = ConsoleColor.Yellow;
            foreach (var item in orderItems)
            {
                Console.WriteLine(item + "\n");

            }
            Console.ForegroundColor = ConsoleColor.White;


            Console.WriteLine("-- Enter address to shipping -- ");
            var address = GetAddress();

            var order = new OrderDto() { OrderAddress = address, UserID = user.Id, Items = orderItems };


            Console.Write("Do you confirm an order?(y/n- go back to menu): ");
            string input = Console.ReadLine();
            if (input == "y")
            {
                user.AddNewOrder(order.Id);
                try
                {
                    OrderService.Add(order);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.ReadKey();
                }
            }
            else
            {
                return;
            }
            Console.Clear();
            Console.WriteLine(OrderService.GetUserOrders(user.Id).Last());
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Your order has been successfully formed");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Press any key go back to menu...");
            Console.ReadKey();
        }



        private void PrintUserOrder(int orderId)
        {
            OrderDto order = OrderService.Find(i => i.Id == orderId);
            Console.WriteLine(order);
        }

        private AddressDto GetAddress()
        {
            var addres = new AddressDto();
            Console.Write("Enter country: ");
            addres.Country = Console.ReadLine();

            Console.Write("Enter city: ");
            addres.City = Console.ReadLine();

            Console.Write("Enter bulding: ");
            addres.Building = Console.ReadLine();

            return addres;
        }

        public override void PrintPoductList()
        {
            base.PrintPoductList();
            Console.WriteLine("Press any key back to menu...");
            Console.ReadKey();
        }

    }
}

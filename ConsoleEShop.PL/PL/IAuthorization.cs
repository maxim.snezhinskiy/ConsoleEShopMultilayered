﻿using ConsoleEShop.BLL.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.PL
{
    public interface IAuthorization
    {
        UserDto SignUp(string login, string password);
        bool SignIn(string login, string password);
    }
}

﻿using ConsoleEShop.BLL.DTOs;
using ConsoleEShopMultilayered.DAL.UoW;
using ConsoleEShopMultilayered.PL;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered
{
    public  class EShopController
    {
        public  UserDto currentUser { get; set; }
        public  IMenu menu { get; set; }

        private UnitOfWork uow;

        public EShopController(UnitOfWork uow)
        {
            this.uow = uow;
        }
        public  void Start() 
        {
            menu = new GuestMenu(uow);
            while (true)
            {
                Console.Clear();

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("\t\t\t\t\t------ ConsoleEShop-Low ------");
                Console.ResetColor();

                menu.LoginNotify += LogIn;
                menu.Execute();
            }
        }

        public  void LogIn(object sender, EventArgs e)
        {
            currentUser = sender as UserDto;
            if (currentUser.Permission == UserPermissionDto.RegisteredUser)
            {
                currentUser.Permission = UserPermissionDto.RegisteredUser;
                menu = new UserMenu(uow, currentUser);
                menu.LogOutNotify += LogOut;
            }
            if (currentUser.Permission == UserPermissionDto.Admin)
            {
                currentUser.Permission = UserPermissionDto.Admin;
                menu = new AdminMenu(uow, currentUser);
                menu.LogOutNotify += LogOut;
            }
        }

        public  void LogOut(object sender, EventArgs e)
        {
            menu = new GuestMenu(uow);
        }

    }
}

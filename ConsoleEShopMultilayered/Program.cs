﻿using System;
using System.Linq;
using System.IO;
using System.Diagnostics.CodeAnalysis;
using System.Collections.Generic;
using ConsoleEShopMultilayered.DAL.UoW;
using ConsoleEShopMultilayered.DAL.Entities;

namespace ConsoleEShopMultilayered

{
    class Program
    {


        static void Main(string[] args)
        {


            var eShopController = new EShopController(new UnitOfWork());
            eShopController.Start();
        }
    }
}

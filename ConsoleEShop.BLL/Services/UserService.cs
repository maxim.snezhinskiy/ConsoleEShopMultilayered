﻿using AutoMapper;
using ConsoleEShop.BLL.DTOs;
using ConsoleEShopMultilayered.BLL.Interfaces;
using ConsoleEShopMultilayered.DAL.Entities;
using ConsoleEShopMultilayered.DAL.UoW;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.BLL.Services
{
    public class UserService : IUserService
    {
        public IUnitOfWork UoW { get; set; }
        private IMapper mapper;
        public UserService(IUnitOfWork uow)
        {
            if (uow == null)
            {
                throw new ArgumentNullException();
            }
            UoW = uow;
            mapper = GetMapperConfiguration().CreateMapper();
        }

        public MapperConfiguration GetMapperConfiguration()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<User, UserDto>();
                cfg.CreateMap<UserDto, User>();
                cfg.CreateMap<Contact, ContactDto>(); 
                cfg.CreateMap<ContactDto, Contact>(); 
                cfg.CreateMap<UserPermissionDto, UserPermission>(); 
                cfg.CreateMap<UserPermission, UserPermissionDto>(); 
            });
        }


        public void Add(UserDto user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(Add));
            }
            UoW.Users.Create(mapper.Map<User>(user));
        }

        public UserDto Find(Predicate<User> p)
        {
            if (p == null)
            {
                throw new ArgumentNullException(nameof(Add));
            }
            return mapper.Map<UserDto>(UoW.Users.Find(p));
        }

        public IEnumerable<UserDto> Get()
        {
            return mapper.Map<IEnumerable<UserDto>>(UoW.Users.Get());
        }

        public UserDto Get(int id)
        {
            return mapper.Map<UserDto>(UoW.Users.Get(id));
        }

        public UserDto LogIn(string login, string password)
        {
            User exist = UoW.Users.Find(i => i.Login == login && i.Password == password);
            if (exist == null)
            {
                throw new Exception("Wrong login or password");
            }
            return mapper.Map<UserDto>(exist);
        }

        public void Register(UserDto user)
        {
            User exist = UoW.Users.Find(i => i.Login == user.Login && i.Password == user.Password);
            if (exist != null)
            {
                throw new Exception("User with this login is already exist");
            }
            UoW.Users.Create(mapper.Map<User>( user));
        }

        public bool AddOrder(int userId)
        {
            throw new NotImplementedException();
        }


        public void Update(UserDto user)
        {
            var mappedUser = mapper.Map<User>(user);
            UoW.Users.Update(mappedUser);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ConsoleEShopMultilayered.BLL.Interfaces;
using ConsoleEShopMultilayered.DAL.UoW;
using ConsoleEShopMultilayered.DAL.Entities;
using AutoMapper;
using ConsoleEShop.BLL.DTOs;

namespace ConsoleEShopMultilayered.BLL.Services
{
    public class OrderService : IOrderService
    {
        public IUnitOfWork UoW { get; set; }
        readonly IMapper mapper;
        public OrderService(IUnitOfWork uow)
        {
            UoW = uow;
            mapper = GetMapperConfiguration().CreateMapper();
        }

        public MapperConfiguration GetMapperConfiguration() 
        {
            return new MapperConfiguration(cfg=> {
                cfg.CreateMap<Order, OrderDto>();
                cfg.CreateMap<OrderDto, Order >();
                cfg.CreateMap<Address, AddressDto>();
                cfg.CreateMap<AddressDto, Address>();
                cfg.CreateMap<OrderItemDto, OrderItem>();
                cfg.CreateMap<OrderItem, OrderItemDto>();
                cfg.CreateMap<ProductDto, Product>();
                cfg.CreateMap<Product, ProductDto>();
            });
        }

        public void Add(OrderDto order)
        {
            if(order == null)
            {
                throw new ArgumentNullException(nameof(Add));
            }

            UoW.Orders.Create(mapper.Map<Order>(order));
        }

        public void AddOrderItem(int orderId, OrderItemDto item)
        {
            if(item == null)
            {
                throw new ArgumentNullException(nameof(AddOrderItem));
            }
            var found = UoW.Orders.Get(orderId);
            if (found == null)
            {
                throw new ArgumentNullException(nameof(AddOrderItem));
            }
            found.Items.Add(mapper.Map<OrderItem>(item));
        }

        public IEnumerable<OrderDto> GetUserOrders(int userId)
        {
            var orders = UoW.Orders.Get().Where(i => i.UserID == userId);
            if (orders == null) return null;
            var ordersDto = new List<OrderDto>();
            foreach (var order in orders)
            {
                ordersDto.Add(mapper.Map<OrderDto>(order));
            }
            return ordersDto;
        }

        public IEnumerable<OrderDto> Get()
        {
            return mapper.Map<IEnumerable<OrderDto>>( UoW.Orders.Get());
        }


        public OrderDto Find(Predicate<Order> p)
        {
           var order = UoW.Orders.Find(p);
            return mapper.Map<OrderDto>(order);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ConsoleEShopMultilayered.BLL.Interfaces;
using ConsoleEShopMultilayered.DAL.UoW;
using ConsoleEShopMultilayered.DAL.Entities;
using ConsoleEShop.BLL.DTOs;
using AutoMapper;

namespace ConsoleEShopMultilayered.BLL.Services
{
    public class ProductService : IProductService

    {
        public IUnitOfWork UoW { get; set; }
        private IMapper mapper;
        public ProductService(IUnitOfWork uow)
        {
            if (uow == null)
            {
                throw new ArgumentNullException();
            }
            UoW = uow;
            mapper = GetMapperConfiguration().CreateMapper();
        }

        public MapperConfiguration GetMapperConfiguration()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Product, ProductDto>();
                cfg.CreateMap<ProductDto, Product>();
            });
        }

        public bool Add(ProductDto product)
        {
            if (product == null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            return UoW.Products.Create(mapper.Map<Product>(product));
        }

        public ProductDto Find(Predicate<Product> p)
        {
            if (p == null)
            {
                throw new ArgumentNullException(nameof(Find));
            }
            var pr = UoW.Products.Find(p);
            return mapper.Map<Product, ProductDto>(pr);
        }

        public ProductDto Get(int id)
        {
            return mapper.Map<ProductDto>(UoW.Products.Get(id));
        }

        public IEnumerable<ProductDto> GetAll()
        {
            return mapper.Map<IEnumerable<ProductDto>>(UoW.Products.Get());
        }
    }
}

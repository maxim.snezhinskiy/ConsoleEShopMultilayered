﻿using ConsoleEShopMultilayered.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ConsoleEShop.BLL.DTOs
{
    public class OrderDto
    {
        public int UserID { get;  set; }
        public int Id { get; set; }
        public AddressDto OrderAddress { get; set; }
        public List<OrderItemDto> Items { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public bool isConfirmed { get; set; }

        public override string ToString()
        {
            string items = "";
            foreach (var i in Items)
            {
                items += i + "\n";
            }
            return $"-- Order №{this.Id}--\n" +
                   $"{items}\n" +
                   $"Order status: {OrderStatus.ToString()}\n" +
                   $"{OrderAddress}" +
                   $"\n-- Total price: {Items.Sum(i => i.Product.Price * i.Quantity)}\n";
        }
    }
}

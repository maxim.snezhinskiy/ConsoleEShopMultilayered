﻿using ConsoleEShopMultilayered.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop.BLL.DTOs
{
    public enum UserPermissionDto
    {
        Admin,
        RegisteredUser
    }

    public class UserDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public UserPermissionDto Permission { get; set; }
        public ContactDto contact { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public List<int> OrdersId { get; set; }

        public void AddNewOrder(int orderId)
        {
            if (OrdersId == null)
                OrdersId = new List<int>();
            OrdersId.Add(orderId);

        }

        public override string ToString()
        {
            return $"User ID: {Id}\n" +
                $"Name: {Name ?? "Unknown"}\n" +
                $"Login: {Login}\n" +
                $"{contact?.ToString()}\n";
        }

    }
}

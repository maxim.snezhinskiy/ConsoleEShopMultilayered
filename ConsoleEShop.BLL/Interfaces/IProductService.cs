﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ConsoleEShopMultilayered.DAL.UoW;
using ConsoleEShopMultilayered.DAL.Entities;
using ConsoleEShop.BLL.DTOs;

namespace ConsoleEShopMultilayered.BLL.Interfaces
{
    public interface IProductService 
    {
        public IUnitOfWork UoW { get; set; }

        public bool Add(ProductDto product);
        public ProductDto Find(Predicate<Product> p);
        public ProductDto Get(int id);
        public IEnumerable<ProductDto> GetAll();
    }
}

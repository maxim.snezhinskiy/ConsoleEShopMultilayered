﻿using ConsoleEShop.BLL.DTOs;
using ConsoleEShopMultilayered.DAL.Entities;
using ConsoleEShopMultilayered.DAL.UoW;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.BLL.Interfaces
{
    public interface IUserService
    {
        public IUnitOfWork UoW { get; set; }
        public void Add(UserDto user);
        public IEnumerable<UserDto> Get();
        public UserDto Get(int id);
        public UserDto Find(Predicate<User> p);
        public UserDto LogIn(string login, string password);
        public void Register(UserDto user);
        public void Update(UserDto user);
        public bool AddOrder(int userId);
    }
}

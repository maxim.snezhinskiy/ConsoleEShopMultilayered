﻿using ConsoleEShopMultilayered.DAL.Entities;
using ConsoleEShopMultilayered.DAL.UoW;
using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using ConsoleEShop.BLL.DTOs;

namespace ConsoleEShopMultilayered.BLL.Interfaces
{
    public interface IOrderService
    {
        public IUnitOfWork UoW { get; set; }
        public void Add(OrderDto order);
        public void AddOrderItem(int orderId, OrderItemDto item);
        public IEnumerable<OrderDto> Get();
        public OrderDto Find(Predicate<Order> p);
        public IEnumerable<OrderDto> GetUserOrders(int userId);
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleEShopMultilayered.DAL.Entities;

namespace ConsoleEShopMultilayered.DAL.Repositories
{
    public interface IRepository<T>
        where T: IEntity
    {
        bool Create(T item);
        T Get(int id);

        List<T> Get();
        void Update(T item);

        void Delete(int id);

        T Find(Predicate<T> p);

    }
}

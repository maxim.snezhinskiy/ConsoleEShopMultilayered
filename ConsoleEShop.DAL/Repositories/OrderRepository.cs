﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ConsoleEShopMultilayered.DAL.Entities;
using ConsoleEShopMultilayered.DAL.Infrastructure;

namespace ConsoleEShopMultilayered.DAL.Repositories
{
    public class OrderRepository : IRepository<Order>
    {
        public DbContext db;

        public int Count => db.Orders.Count;
    
        public OrderRepository(DbContext context)
        {
            db = context;
        }
  
        public Order Find(Predicate<Order> p) 
        {
            return db.Orders.Find(p);
        }

        public bool Create(Order item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("", new Exception());
            }
            if (db.Orders.Contains(item))
                return false;

            item.Id = IDGenerator.GenerateID(db.Orders);
            db.Orders.Add(item);
            return true;
        }

        public Order Get(int id)
        {
            return db.Orders.FirstOrDefault(i=> i.Id == id);
        }

        public List<Order> Get()
        {
            return db.Orders;
        }

        public void Update(Order item)
        {
            int indexToUpdate = db.Orders.Select(x => x.Id).FirstOrDefault(x => x == item.Id) - 1;

            if (indexToUpdate != -1)
            {
                db.Orders[indexToUpdate] = item;
            }

        }

        public void Delete(int id)
        {
            if (!(db.Orders.Exists(i => i.Id == id)))
            {
                throw new ArgumentException(nameof(this.Delete), new Exception("User with such ID is not exist"));
            }
            db.Orders.Remove(db.Orders.Find(i => i.Id == id));
        }
    }
}

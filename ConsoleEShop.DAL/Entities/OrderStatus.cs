﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Entities
{
    public enum OrderStatus
    {
        New,
        CanceledByAdmin,
        CanceledByUser,
        ReceivedPayment,
        Sent,
        Completed
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ConsoleEShopMultilayered.DAL.Entities
{
    public class Order : IEntity
    {
        public int Id { get; set; }
        public Address OrderAddress { get; set; } = new Address();
        public List<OrderItem> Items { get; set; } = new List<OrderItem>();
        public OrderStatus OrderStatus { get; set; }
        public bool isConfirmed { get; set; }

        public int UserID { get; set; }

    }
}

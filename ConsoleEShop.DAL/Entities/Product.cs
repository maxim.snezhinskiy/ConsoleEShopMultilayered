﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Entities
{
    public class Product : IEntity
    {
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public int Id { get; set; }
    }
}

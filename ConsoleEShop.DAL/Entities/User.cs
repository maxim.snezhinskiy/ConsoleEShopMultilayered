﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Entities
{
    public class User : IEntity
    {
        public int Id { get;  set; }
        public string Name { get; set; }
        public List<int> OrdersId { get; set; } 
        public UserPermission Permission { get;   set; }
        public Contact contact { get; set; } = new Contact();
        public string Login { get; set; }
        public string Password { get; set; }
    }
}

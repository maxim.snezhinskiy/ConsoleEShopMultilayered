﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Entities
{
    public class OrderItem
    {
        public int Quantity { get; set; }
        public Product Product { get; set; }
    }
}

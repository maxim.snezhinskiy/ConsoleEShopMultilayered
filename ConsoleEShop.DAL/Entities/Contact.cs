﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Entities
{
    public class Contact
    {
        public string PhoneNumber { get; set; }
        public string Email { get; set; }

    }
}

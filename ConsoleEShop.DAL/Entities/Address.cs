﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Entities
{
    public class Address
    {
        public string Country { get; set; }
        public string City { get; set; }
        public string Building { get; set; }
        public override string ToString() => $"{Country}, {City} city, building {Building}";
    }
}

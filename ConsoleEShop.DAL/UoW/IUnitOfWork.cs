﻿using ConsoleEShopMultilayered.DAL.Entities;
using ConsoleEShopMultilayered.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.UoW
{
    public interface IUnitOfWork
    {
        public IRepository<Order> Orders { get;}
        public IRepository<User> Users { get;}
        public IRepository<Product> Products { get;}
    }
}

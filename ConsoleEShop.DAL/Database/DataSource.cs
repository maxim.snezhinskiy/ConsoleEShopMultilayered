﻿using ConsoleEShopMultilayered.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered
{
    public class DataSource
    {
        public static List<Product> Products { get; set; } = new List<Product>()
        {
             new Product(){ Id = 1, ProductName= "Iphone 12", Price=1199, Description = "Apple Iphone", Category = "Phones"},
             new Product(){ Id = 2, ProductName= "Samsung Galaxy s41", Price=999, Description = "Samsung corporation", Category = "Phones"},
             new Product(){ Id = 3, ProductName= "Mercedes benz E-200", Price=58999, Description = "The best car ever",  Category = "Cars"},
             new Product(){ Id = 4, ProductName= "Apple airPods", Price=199, Description = "Apple Inc", Category = "HeadPhones"}
        };

        public static List<User> Users { get; set; } = new List<User>()
        {
            new User(){ Id=1, Login= "simpleUSer", Password = "password", Permission=UserPermission.RegisteredUser},
            new User(){ Id=2, Login = "user1", Password = "user1" , Permission=UserPermission.RegisteredUser},
            new User(){ Id=3, Login="admin", Password="admin", Permission=UserPermission.Admin }
        };

        public static List<Order> Orders { get; set; } = new List<Order>();
    }
}

﻿using ConsoleEShopMultilayered.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered
{
    public class DbContext
    {
        public virtual List<User> Users { get; set; }
        public virtual List<Order> Orders { get; set; }
        public virtual List<Product> Products { get; set; }

        public DbContext() 
        {
            Users = DataSource.Users;
            Orders = DataSource.Orders;
            Products = DataSource.Products;
        }
    }
}
